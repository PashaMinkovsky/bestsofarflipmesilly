//
//  LoginViewController.swift
//  FlipMeSilly
//
//  Created by Pasha on 10/11/16.
//  Copyright © 2016 Pasha. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        email.attributedPlaceholder = NSAttributedString(string:"Email:",attributes:[NSForegroundColorAttributeName: UIColor.init(white: 0.95, alpha: 0.65)])
        password.attributedPlaceholder = NSAttributedString(string:"Password:",attributes:[NSForegroundColorAttributeName: UIColor.init(white: 0.95, alpha: 0.65)])
        
        email.delegate = self
        password.delegate = self

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func EmailNext(_ sender: AnyObject) {
        let nextField = password.superview?.viewWithTag(1) as? UITextField
        nextField?.becomeFirstResponder()
    }
    
    @IBAction func PasswordGoPressed(_ sender: AnyObject) {
        login()
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let border = CALayer()
        let width = CGFloat(0.7)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: email.frame.size.height - width, width:  email.frame.size.width, height: email.frame.size.height)
        
        border.borderWidth = width
        email.layer.addSublayer(border)
        email.layer.masksToBounds = true
        
        let border1 = CALayer()
        let width1 = CGFloat(0.7)
        border1.borderColor = UIColor.white.cgColor
        border1.frame = CGRect(x: 0, y: password.frame.size.height - width1, width:  password.frame.size.width, height: password.frame.size.height)
        
        border1.borderWidth = width
        password.layer.addSublayer(border1)
        password.layer.masksToBounds = true
    }
    
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBAction func loginPressed(_ sender: AnyObject) {
        
        login()
//        UserDefaults.standard.set(1, forKey: "randomQuote") //for random quotes
//        
//        if ((email.text?.isEmpty)! || (password.text?.isEmpty)!) {
//            throwAlert("Please fill in all fields", title: "You left something blank")
//            return
//        }
//        
//        let userEmail = email.text!
//        let userPassword = password.text!
//        
//        FIRAuth.auth()?.signIn(withEmail: userEmail, password: userPassword) { (user, error) in
//            if (error != nil) {
//                //throw error
//                self.throwAlert("Wrong password/login combination", title: "Login not successful")
//            } else {
//                self.view.endEditing(true)
//                let myViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabController")
//                
//                let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                appDelegate.window?.rootViewController = myViewController
//                appDelegate.window?.makeKeyAndVisible()
//            }
//        }
    }
    
    
    func login() {
        UserDefaults.standard.set(1, forKey: "randomQuote") //for random quotes
        
        if ((email.text?.isEmpty)! || (password.text?.isEmpty)!) {
            throwAlert("Please fill in all fields", title: "You left something blank")
            return
        }
        
        let userEmail = email.text!
        let userPassword = password.text!
        
        FIRAuth.auth()?.signIn(withEmail: userEmail, password: userPassword) { (user, error) in
            if (error != nil) {
                //throw error
                self.throwAlert("Wrong password/login combination", title: "Login not successful")
            } else {
                self.view.endEditing(true)
                let myViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabController")
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = myViewController
                appDelegate.window?.makeKeyAndVisible()
            }
        }
    }
    
    
    
    func throwAlert(_ message:String, title: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }

}
