//
//  RegisterPageViewController.swift
//  FlipMeSilly
//
//  Created by Pasha on 10/9/16.
//  Copyright © 2016 Pasha. All rights reserved.
//

import UIKit
import Firebase

class RegisterPageViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        //
        userEmailTextField.attributedPlaceholder = NSAttributedString(string:"Email:",attributes:[NSForegroundColorAttributeName: UIColor.init(white: 0.95, alpha: 0.65)])
        userPasswordTextField.attributedPlaceholder = NSAttributedString(string:"Password:",attributes:[NSForegroundColorAttributeName: UIColor.init(white: 0.95, alpha: 0.65)])
        userPasswordRepeatedTextField.attributedPlaceholder = NSAttributedString(string:"Repeat Password:",attributes:[NSForegroundColorAttributeName: UIColor.init(white: 0.95, alpha: 0.65)])
        
        userEmailTextField.delegate = self
        userPasswordTextField.delegate = self
        userPasswordRepeatedTextField.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let border = CALayer()
        let width = CGFloat(0.7)
        border.borderColor = UIColor.white.cgColor
        border.frame = CGRect(x: 0, y: userEmailTextField.frame.size.height - width, width:  userEmailTextField.frame.size.width, height: userEmailTextField.frame.size.height)
        border.borderWidth = width
        userEmailTextField.layer.addSublayer(border)
        userEmailTextField.layer.masksToBounds = true
        
        let border1 = CALayer()
        let width1 = CGFloat(0.7)
        border1.borderColor = UIColor.white.cgColor
        border1.frame = CGRect(x: 0, y: userPasswordTextField.frame.size.height - width1, width:  userPasswordTextField.frame.size.width, height: userPasswordTextField.frame.size.height)
        border1.borderWidth = width
        userPasswordTextField.layer.addSublayer(border1)
        userPasswordTextField.layer.masksToBounds = true
        
        let border12 = CALayer()
        let width12 = CGFloat(0.7)
        border12.borderColor = UIColor.white.cgColor
        border12.frame = CGRect(x: 0, y: userPasswordRepeatedTextField.frame.size.height - width12, width:  userPasswordRepeatedTextField.frame.size.width, height: userPasswordRepeatedTextField.frame.size.height)
        border12.borderWidth = width
        userPasswordRepeatedTextField.layer.addSublayer(border12)
        userPasswordTextField.layer.masksToBounds = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var userEmailTextField: UITextField!
    @IBOutlet weak var userPasswordTextField: UITextField!
    @IBOutlet weak var userPasswordRepeatedTextField: UITextField!
    
    
    @IBAction func FirstTextFieldNextPressed(_ sender: AnyObject) {
        let nextField = userEmailTextField.superview?.viewWithTag(1) as? UITextField
        nextField?.becomeFirstResponder()
        
    }
    @IBAction func SecondTextFieldNextPressed(_ sender: AnyObject) {
        let nextField = userPasswordRepeatedTextField.superview?.viewWithTag(2) as? UITextField
        nextField?.becomeFirstResponder()
    }
    @IBAction func EnterPressed(_ sender: AnyObject) {
        register()
    }
    
    @IBAction func registerButtonPressed(_ sender: AnyObject) {
        register()
//        if ((userEmailTextField.text?.isEmpty)! || (userPasswordTextField.text?.isEmpty)! || (userPasswordRepeatedTextField.text?.isEmpty)!) {
//            throwAlert("Please fill in every field.", title: "You left a field blank.")
//            return
//        }
//        
//        UserDefaults.standard.set(0, forKey: "currentInvestment")
//        UserDefaults.standard.set(0, forKey: "totalProfit")
//        
//        let userEmail = userEmailTextField!.text!
//        let userPassword = userPasswordTextField!.text!
//        let userPasswordRepeat = userPasswordRepeatedTextField.text
//        
//        if (userPassword != userPasswordRepeat) {
//            throwAlert("Please retype your password.", title: "Your passwords don't match. ")
//            return
//        }
//        
//        //upload to firebase
//        FIRAuth.auth()?.createUser(withEmail: userEmail, password: userPassword) { (user, error) in
//            if (error != nil) { //errored
//                //throw alert
//                print("alert")
//                print(error)
//                self.throwAlert("Please try again later", title: "Registration unsuccessful")
//            } else { //successful
//                let ref = FIRDatabase.database().reference()
//                let Email = userEmail
//                let Investment = 0
//                let Profit = 0
//                let itemDict = ["Email" : Email, "Investment" : ["Investment" : Investment], "TotalProfit" : ["TotalProfit" : Profit]] as [String : Any]
//                ref.child("users").child((user?.uid)!).setValue(itemDict)
//                ref.child("users").observeSingleEvent(of: (.value), with: { (snapshot) in
//                    print(snapshot)
//                })
//                //successfully created user
//                UserDefaults.standard.set(true, forKey: "Registered?")
//                self.view.endEditing(true)
//                self.login()
//                return
//            }
//        }
        
    }
    
    func register() {
        if ((userEmailTextField.text?.isEmpty)! || (userPasswordTextField.text?.isEmpty)! || (userPasswordRepeatedTextField.text?.isEmpty)!) {
            throwAlert("Please fill in every field.", title: "You left a field blank.")
            return
        }
        
        UserDefaults.standard.set(0, forKey: "currentInvestment")
        UserDefaults.standard.set(0, forKey: "totalProfit")
        
        let userEmail = userEmailTextField!.text!
        let userPassword = userPasswordTextField!.text!
        let userPasswordRepeat = userPasswordRepeatedTextField.text
        
        if (userPassword != userPasswordRepeat) {
            throwAlert("Please retype your password.", title: "Your passwords don't match. ")
            return
        }
        
        //upload to firebase
        FIRAuth.auth()?.createUser(withEmail: userEmail, password: userPassword) { (user, error) in
            if (error != nil) { //errored
                //throw alert
                print("alert")
                print(error)
                self.throwAlert("Password must be at least 6 characters long.", title: "Registration unsuccessful.")
            } else { //successful
                let ref = FIRDatabase.database().reference()
                let Email = userEmail
                let Investment = 0
                let Profit = 0
                let itemDict = ["Email" : Email, "Investment" : ["Investment" : Investment], "TotalProfit" : ["TotalProfit" : Profit]] as [String : Any]
                ref.child("users").child((user?.uid)!).setValue(itemDict)
                ref.child("users").observeSingleEvent(of: (.value), with: { (snapshot) in
                    print(snapshot)
                })
                //successfully created user
                UserDefaults.standard.set(true, forKey: "Registered?")
                self.view.endEditing(true)
                self.login()
                return
            }
        }

    }
    
    
    @IBAction func alreadyHaveAccountPressed(_ sender: AnyObject) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func login() {
        let userEmail = userEmailTextField!.text!
        let userPassword = userPasswordTextField!.text!
        FIRAuth.auth()?.signIn(withEmail: userEmail, password: userPassword) { (user, error) in
            if (error != nil) {
                //throw error
                self.throwAlert("Wrong password/login combination", title: "Login not successful")
            } else {
                let myViewController = self.storyboard!.instantiateViewController(withIdentifier: "TabController")
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = myViewController
                appDelegate.window?.makeKeyAndVisible()
            }
        }
    }
    
    
    
    
    func throwAlert(_ message:String, title: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        
        alert.addAction(okAction)
        
        self.present(alert, animated: true, completion: nil)
    }
    

}
