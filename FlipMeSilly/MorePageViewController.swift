//
//  MorePageViewController.swift
//  FlipMeSilly
//
//  Created by Pasha on 10/12/16.
//  Copyright © 2016 Pasha. All rights reserved.
//

import UIKit

class MorePageViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var array = ["About Us", "Terms and Conditions"]
    @IBOutlet weak var tableView: UITableView!

    //Table view methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell")!
        
        cell.textLabel?.text = array[indexPath.row]
        
        return cell
    }
    
    
    @IBAction func LogOutPressed(_ sender: AnyObject) {
        
        UserDefaults.standard.set(false, forKey: "userLoggedIn")
        
        UserDefaults.standard.synchronize()
        
        let loginViewController = self.storyboard!.instantiateViewController(withIdentifier: "loginView") as! LoginViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = loginViewController
        
        appDelegate.window?.makeKeyAndVisible()
        
    }
    

}
