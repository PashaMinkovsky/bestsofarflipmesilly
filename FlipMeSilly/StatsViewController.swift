//
//  StatsViewController.swift
//  FlipMeSilly
//
//  Created by Pasha on 10/12/16.
//  Copyright © 2016 Pasha. All rights reserved.
//

import UIKit
import Firebase

class StatsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBOutlet weak var currentInvestment: UILabel!
    
    @IBOutlet weak var currentProfit: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        var ref: FIRDatabaseReference
        ref = FIRDatabase.database().reference()
        ref.child("users/\(FIRAuth.auth()!.currentUser!.uid)/Investment").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            var dataDict = snapshot.value as! [String: AnyObject]
            self.currentInvestment.text = String(dataDict["Investment"] as! Int)
        })
        ref.child("users/\(FIRAuth.auth()!.currentUser!.uid)/TotalProfit").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
            var dataDict = snapshot.value as! [String: AnyObject]
            self.currentProfit.text = String(dataDict["TotalProfit"] as! Int)
        })
    }
}
