//
//  ViewController.swift
//  FlipMeSilly
//
//  Created by Pasha on 10/8/16.
//  Copyright © 2016 Pasha. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController, UIPopoverPresentationControllerDelegate, UITextFieldDelegate{

    override func viewDidLoad() {
        super.viewDidLoad()
        moneyAmount.delegate = self
        let quoteArray = ["'In investing, what is comfortable is rarely profitable.' - Robert Arnott", "We don’t have to be smarter than the rest. We have to be more disciplined than the rest. - Warren Buffet", "'Wide diversification is only required when investors do not understand what they are doing.' - Warren Buffett", "'Calling someone who trades actively in the market an investor is like calling someone who repeatedly engages in one-night stands a romantic.' - Warren Buffett", "'Generally, the greater the stigma or revulsion, the better the bargain.' - Seth Klarman"]
        var index = 0
        for item in quoteArray {
            quoteDictionary[index] = item
            index += 1
            self.hideKeyboardWhenTappedAround()
        }
        
        let randomNumber = UserDefaults.standard.integer(forKey: "randomQuote")
        quoteLabel.text = quoteDictionary[randomNumber]
        quoteLabel.textColor = UIColor(white: 0, alpha: 1)
        UserDefaults.standard.set((randomNumber+1) % 3, forKey: "randomQuote")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    var flipLabel = "flip"
    var moneyWhenPressed = ""
    var quoteDictionary : [Int:String] = [:]

    @IBOutlet weak var moneyAmount: UITextField!
    
    @IBOutlet weak var warningLabel: UILabel!

    @IBOutlet weak var warningLabel2: UILabel!
    
    @IBOutlet weak var FlipButton: UIButton!
    
    @IBOutlet weak var quoteLabel: UILabel!
    
    @IBAction func FlipPressed(_ sender: AnyObject) {
        var ref: FIRDatabaseReference
        ref = FIRDatabase.database().reference()
        
        if (flipLabel == "flip") {
            if (moneyAmount.text == "" || (Int(moneyAmount.text!)! - 0 == 0)) {
                throwAlert("", title: "Please enter a nonzero amount.")
                return
            }
            moneyWhenPressed = moneyAmount.text!
            FlipButton.setTitle("Confirm", for: UIControlState())
            FlipButton.backgroundColor = UIColor.init(red: 126/255, green: 222/255, blue: 37/255, alpha: 1)
            warningLabel.text = "You are about to invest $" + moneyAmount.text! + " from your venmo."
            warningLabel.textColor = UIColor(white: 0, alpha: 1)
            warningLabel2.textColor = UIColor(white: 0, alpha: 1)
            flipLabel = "confirm"
            view.endEditing(true)
        } else { //flipLabel is "confirm"
            if (moneyWhenPressed != moneyAmount.text!) { //money amount changed
                throwAlert("Please do not edit the investment amount after pressing 'Flip!'", title: "Investment amount inconsistent.")
                moneyAmount.text = ""
                flipLabel = "flip"
                FlipButton.setTitle("Flip!", for: UIControlState())
                FlipButton.backgroundColor = UIColor.init(red: 61/255, green: 177/255, blue: 255/255, alpha: 1)
                warningLabel.text = ""
                warningLabel.textColor = UIColor(white: 0, alpha: 0)
                warningLabel2.textColor = UIColor(white: 0, alpha: 0)
                return
            }
            //All the logistics worked out
            //Saving to database
            let newMoney = Int(moneyAmount.text!)!
            var currentInvestmentFromDB = 0
            
            ref.child("users/\(FIRAuth.auth()!.currentUser!.uid)/Investment").observeSingleEvent(of: FIRDataEventType.value, with: { (snapshot) in
                var dataDict = snapshot.value as! [String: AnyObject]
                currentInvestmentFromDB = dataDict["Investment"] as! Int
                let newInvestment = currentInvestmentFromDB + newMoney
                ref.child("users/\(FIRAuth.auth()!.currentUser!.uid)/Investment").setValue(["Investment": newInvestment])
                self.throwAlert("Your current balance is $\(newInvestment).", title: "Investment Successful!")
            })
            
            warningLabel.text = ""
            warningLabel.textColor = UIColor(white: 0, alpha: 0)
            warningLabel2.textColor = UIColor(white: 0, alpha: 0)
//            let currentSaved = UserDefaults.standard.integer(forKey: "currentInvestment")

            //let newMoney = Int(moneyAmount.text!)!
//            UserDefaults.standard.set(currentSaved + newMoney, forKey: "currentInvestment")
            moneyAmount.text = ""
            flipLabel = "flip"
            FlipButton.setTitle("Flip!", for: UIControlState())
            FlipButton.backgroundColor = UIColor.init(red: 61/255, green: 177/255, blue: 255/255, alpha: 1)
        }
    }
    
    @IBAction func RealLogoutPressed(_ sender: AnyObject) {
        UserDefaults.standard.set(false, forKey: "userLoggedIn")
        
        UserDefaults.standard.synchronize()
        
        let loginViewController = self.storyboard!.instantiateViewController(withIdentifier: "loginView") as! LoginViewController
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.window?.rootViewController = loginViewController
        
        appDelegate.window?.makeKeyAndVisible()
    }
    
    
//    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
//        if (string == "0" || string == "1" || string == "2" || string == "3" || string == "4" || string == "5" || string == "6" || string == "7" || string == "8" || string == "9") {
//            return true
//        } else {
//            return false
//        }
//    }
    
    func throwAlert(_ message:String, title: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    

}

